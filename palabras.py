
# Función: Expresión palabras ADN

def expresion_adn(secuencia, palabra):

    # 1. Cálculo de la frecuencia

    # 1.1 Obtener las palabras de la secuencia del tamaño de 'palabra'

    palabras = []

    k = len(palabra)    # longitud palabra

    # Recorremos las letras sin llegar hasta el final, nos quedamos k letras antes
    for i in range(len(secuencia) - k + 1):
        fragmento = secuencia[i:i + k]
        palabras.append(fragmento)


    # 1.2 Calcular las apariciones de 'palabra' en 'palabras'

    apariciones = palabras.count(palabra)

    frecuencia = apariciones / len(palabras)


    # 2. Cálculo de la probabilidad de 'palabra'

    # Utilizamos un diccionario que asocie para cada base un contador con las veces que aparece

    contadores = {"A": 0, "C": 0, "G": 0, "T": 0}

    # Cada vez que encontramos una letra, incrementamos su contador
    for letra in secuencia:
        contadores[letra] = contadores[letra] + 1

    # Con los contadores calculamos la probabilidad de cada base
    # De nuevo, almacenamos esa información en un diccionario

    probabilidades = {}

    for base in contadores:
        probabilidades[base] = contadores[base] / len(secuencia)


    # probabilidad de 'palabra' en 'secuencia'
    # Consiste en multiplicar la probabilidad de cada una de sus letras

    probabilidad = 1
    for letra in palabra:
        probabilidad = probabilidad * probabilidades[letra] 

    # 3. Cálculo de la expresión

    expresion = frecuencia / probabilidad

    return expresion


# pruebas

resultado1 = expresion_adn("ACCTGAAC", "AC")

print(resultado1)

resultado2 = expresion_adn("CCTATCGGTGGATTAGCATGTCCCTGTACGTTTCGCCGCGAACTAGTTCACACGGCTTGATGGCAAATGGTTTTTCCGGCGACCGTAATCGTCCACCGAG", "GT")

print(resultado2)

