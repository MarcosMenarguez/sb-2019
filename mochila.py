
# Problema de la mochila, avance rápido

from itertools import combinations, chain

# Función de soporte que calcula el conjunto potencia: todos los subconjuntos de una colección
def powerset(elementos):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(elementos)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


from collections import namedtuple, deque

def voraz_mochila(mochila, objetos):

    # Variables
    solucion = []
    peso_actual = 0
    objetos_ordenados = deque(sorted(objetos, key= lambda objeto: objeto.valor/objeto.peso, reverse=True))

    while objetos_ordenados and peso_actual < mochila :
        objeto = objetos_ordenados.popleft() # saca y borra el primero

        if peso_actual + objeto.peso <= mochila :
            solucion.append(objeto)
            peso_actual = peso_actual + objeto.peso

    return solucion

def busqueda_exhaustiva_mochila(mochila, objetos):

    soa = []
    voa = -1 # cota inferior, problema maximización

    for candidata in powerset(objetos):
        # candidata es subconjunto de objetos
        peso_actual = 0
        for objeto in candidata:
            peso_actual = peso_actual + objeto.peso

        if peso_actual <= mochila: # ¿Es solución? ¿Cabe en la mochila?
            # Medimos la solución
            valor_solucion = 0
            for objeto in candidata:
                valor_solucion = valor_solucion + objeto.valor
            if valor_solucion > voa:
                soa = candidata
                voa = valor_solucion
    
    return soa


# Pruebas

mochila = 10

Objeto = namedtuple('Objeto', ['nombre', 'peso', 'valor'])

joyeria1 = [
    Objeto(nombre="anillo", peso = 10, valor = 100),
    Objeto(nombre="pulsera", peso = 3, valor = 10),
    Objeto(nombre="reloj", peso = 3, valor = 10),
    Objeto(nombre="pendientes", peso = 3, valor = 10)
]
joyeria2 = [
    Objeto(nombre="anillo", peso = 10, valor = 100),
    Objeto(nombre="pulsera", peso = 3, valor = 90),
    Objeto(nombre="reloj", peso = 3, valor = 90),
    Objeto(nombre="pendientes", peso = 3, valor = 90)
]

# print( voraz_mochila(mochila, joyeria2) )

print( busqueda_exhaustiva_mochila(mochila, joyeria2) )
