
from itertools import product

# motifs

def puntuacion_consenso(motifs, k):

    # 1. Construir la tabla profile con ceros

    profile = {}

    for base in ["A", "C", "G", "T"]:
        lista = [0] * k
        profile[base] = lista

    # for base, lista in diccionario.items():
    #     print(base, ": ", lista)

    # Recorremos todas las bases de los motifs, por columnas
    # Cada vez que encontremos una base, incrementamos su contador

    for columna in range(k):
        for motif in motifs:
            base = motif[columna]
            contador = profile[base][columna]
            profile[base][columna] = contador + 1
    
    # Calculamos la puntuación y la cadena consenso
    # puntuación: suma de los máximos de cada columna
    # consenso: bases de los máximos de cada columna

    puntuacion = 0
    consenso = ""

    # Recorremos la tabla profile por columnas
    for columna in range(k):
        maximo_columna = -1
        base_maximo = ""
        for base in profile:
            contador = profile[base][columna]
            if contador > maximo_columna:
                maximo_columna = contador
                base_maximo = base
        
        # En este punto tenemos calculado el máximo de la columna y su base
        # Actualizamos puntuación y consenso
        puntuacion = puntuacion + maximo_columna
        consenso = consenso + base_maximo

    # Retornamos el resultado del cálculo como una tupla
    return (puntuacion, consenso)


# Pruebas

motifs1 = ["AGGTACTT", "CCATACGT", "ACGTTAGT", "ACGTCCAT", "CCGTACGG"]
k1 = 8

puntuacion1, consenso1 = puntuacion_consenso(motifs1, k1)
# print("Puntuación: ", puntuacion1)
# print("Consenso: ", consenso1)


# Parte 2: búsqueda de los motifs

def busqueda_exhaustiva_motifs(t, k, n, secuencias):
    soa_motifs = []
    soa_posiciones = []
    voa = -1

    for candidata in product(range(n - k + 1), repeat = t):
        motifs = []
        for pos_candidata in range(len(candidata)):
            secuencia = secuencias[pos_candidata]
            indice = candidata[pos_candidata]
            motif = secuencia[indice:indice + k]
            motifs.append(motif)

        puntuacion, consenso = puntuacion_consenso(motifs, k)
        if puntuacion > voa:
            soa_motifs = motifs
            soa_posiciones = candidata
            voa = puntuacion

    lo_mejor = t * k
    voa_relativo = voa/lo_mejor # Valor entre 0 y 1, indica lo buena que es la solución

    return (soa_motifs, soa_posiciones, voa_relativo)

# Pruebas

t=4
k=8
n=32  
secuencias=(
  "GGGCGAGGTATGTGTAAGTGCCAAGGTGCCAG",
  "TAGTACCGAGACCGAAAGAAGTATACAGGCGT",
  "TAGATCAAGTTTCAGGTGCACGTCGGTGAACC",
  "AATCCACCAGCTCCACGTGCAATGTTGGCCTA"
)

motifs, posiciones, valor = busqueda_exhaustiva_motifs(t, k, n, secuencias)

print(motifs)
print(posiciones)
print(valor)


