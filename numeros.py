
# Problema suma números

from itertools import combinations, chain
from collections import deque

# Función de soporte que calcula el conjunto potencia: todos los subconjuntos de una colección
def powerset(elementos):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(elementos)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

# Ejercicio de clase: encuentra todas las soluciones
def suma_numeros(numeros, suma_objetivo):
    soluciones = []

    for candidata in powerset(numeros):
        suma = 0        
        for numero in candidata:  # calcula la suma de la solución candidata
            suma = suma + numero
        if suma == suma_objetivo:
            soluciones.append(candidata)
            # break

    return soluciones

# Prueba

solucion1 = suma_numeros(numeros = [13, 11, 7, 18], suma_objetivo = 18)
# print(solucion1)

# Versión por optimización: encuentra la solución por el conjunto de mayor tamaño
def suma_numeros_optimizacion(numeros, suma_objetivo):
    soa = []    
    voa = - 1 # Todas las soluciones candidatas tendrán un tamaño menor
    for candidata in powerset(numeros):
        suma = 0        
        for numero in candidata:
            suma = suma + numero
        if suma == suma_objetivo and len(candidata) > voa: # ¿es solución y es mejor que soa?
            soa = candidata
            voa = len(candidata)

    return soa    

def suma_numeros_voraz(numeros, suma_objetivo):

     # Variables
    solucion = []
    suma_actual = 0
    numeros_ordenados = deque(sorted(numeros))

    while numeros_ordenados and suma_actual < suma_objetivo :
        numero = numeros_ordenados.popleft() # saca y borra el primero

        if suma_actual + numero <= suma_objetivo :
            solucion.append(numero)
            suma_actual = suma_actual + numero

    if suma_actual == suma_objetivo: # tengo solución
        return solucion
    else:
        return [] # indicación de que no hay solución



# Prueba

solucion = suma_numeros_voraz([13, 11, 7, 18], 19)
print(solucion)

