
# Alineamiento de secuencias

from collections import deque

def alineamiento(v, w):

    n_filas = len(v) + 1
    n_columnas = len(w) + 1
    
    # 1. Inicializamos las tablas de soluciones (s) y decisiones (d)
    s = []
    d = []
    for i in range(n_filas):
        fila = [0] * n_columnas
        fila_decisiones = [""] * n_columnas
        s.append(fila)
        d.append(fila_decisiones)

    # 2. Casos base
    s[0][0] = 0
    for fila in range(1, n_filas):
        s[fila][0] = 0
        d[fila][0] = 'abajo'

    for columna in range(1, n_columnas):
        s[0][columna] = 0
        d[0][columna] = 'derecha'

    # 3. Calculamos el resto de la matriz de soluciones
    for columna in range(1, n_columnas):
        for fila in range(1, n_filas):
            beneficio_abajo = s[fila - 1][columna]
            beneficio_derecha = s[fila][columna - 1]
            beneficio_diagonal = s[fila - 1][columna - 1]

            if v[fila - 1] == w[columna - 1]:
                beneficio_diagonal += 1

            s[fila][columna] = max(beneficio_abajo, beneficio_derecha, beneficio_diagonal)

            if s[fila][columna] == beneficio_abajo:
                d[fila][columna] = 'abajo'
            elif s[fila][columna] == beneficio_derecha:
                d[fila][columna] = 'derecha'
            else:
                d[fila][columna] = 'diagonal'

    # 4. Reconstruye el camino

    fila = n_filas - 1
    columna = n_columnas - 1

    nueva_v = ""
    nueva_w = ""

    while fila != 0 or columna != 0:
        if d[fila][columna] == 'abajo':
            fila = fila - 1
            nueva_v = v[fila] + nueva_v
            nueva_w = "-" + nueva_w            
           
        elif d[fila][columna] == 'derecha':
            columna = columna - 1
            nueva_v = "-" + nueva_v
            nueva_w = w[columna] + nueva_w
            
        else:   # diagonal
            fila = fila - 1
            columna = columna - 1
            nueva_v = v[fila]  + nueva_v
            nueva_w = w[columna] + nueva_w
            

    # 5. La solución es el beneficio y el camino
    return (s[n_filas - 1][n_columnas - 1], nueva_v, nueva_w)

# Pruebas

resultado1, v1, w1 = alineamiento("AGCT", "CATG")

print(resultado1)
print(v1)
print(w1)

