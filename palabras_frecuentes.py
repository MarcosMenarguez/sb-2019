
# Bloque Colecciones: ejercicio palabras frecuentes

def palabras_frecuentes(secuencia, k):

    # Diccionario para almacenar el número de veces que aparece cada fragmento de tamaño k (kmero)
    diccionario = {}
    # Variable que va a contener el valor mayor de apariciones del diccionario (columna values)  
    max_apariciones = 0 
    # Resultado del cálculo: lista de las palabras más frecuentes
    resultado = []

    # 1. Recorrer la secuencia y construir el diccionario de apariciones de k-meros 
    # kmero: fragmento de tamaño k   

    for i in range(len(secuencia) - k + 1):
        kmero = secuencia[i:i + k]
        if kmero in diccionario:
            diccionario[kmero] = diccionario[kmero] + 1
        else:
            diccionario[kmero] = 1
        
        # alternativa:  diccionario[kmero] = diccionario.get(kmero, 0) + 1


    # 2. Recorrer los valores del diccionario (values) para buscar el número máximo de apariciones
      # Este valor será asignado a "max_apariciones".    

    for aparicion in diccionario.values():
        if aparicion > max_apariciones:
            max_apariciones = aparicion

    # 3. Recorrer el diccionario y añadir a la lista "resultado" los k-meros con  "max_apariciones"

    for kmero in diccionario:
        if diccionario[kmero] == max_apariciones:
            resultado.append(kmero)

    return resultado



# Prueba

secuencia="ACGTTGCATGTCGCATGATGCATGAGAGCT"
k=4 

resultado = palabras_frecuentes(secuencia, k)
print(resultado)


