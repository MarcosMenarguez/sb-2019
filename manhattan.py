
# Manhattan por programación dinámica

from collections import deque

def manhattan_dinamica(n_filas, n_columnas, abajo, derecha):

    # 1. Construir tabla de soluciones con ceros
    s = []
    for i in range(n_filas):
        fila = [0] * n_columnas
        s.append(fila)

    # 2. Casos base
    s[0][0] = 0
    for fila in range(1, n_filas):
        s[fila][0] = s[fila - 1][0] + abajo[fila - 1][0]

    for columna in range(1, n_columnas):
        s[0][columna] = s[0][columna - 1] + derecha[0][columna - 1]
    
    # 3. Rellenar el resto de la matriz
    for columna in range(1, n_columnas):
        for fila in range(1, n_filas):
            beneficio_abajo = s[fila - 1][columna] + abajo[fila - 1][columna]
            beneficio_derecha = s[fila][columna - 1] + derecha[fila][columna - 1]

            s[fila][columna] = max(beneficio_abajo, beneficio_derecha)

    # 4. Retornar la solución
    return s[n_filas - 1][n_columnas - 1]

def manhattan_dinamica2(n_filas, n_columnas, abajo, derecha):

    # 1. Inicializamos las tablas de soluciones (s) y decisiones (d)
    s = []
    d = []
    for i in range(n_filas):
        fila = [0] * n_columnas
        fila_decisiones = [""] * n_columnas
        s.append(fila)
        d.append(fila_decisiones)

    # 2. Casos base
    s[0][0] = 0
    for fila in range(1, n_filas):
        s[fila][0] = s[fila - 1][0] + abajo[fila - 1][0]
        d[fila][0] = 'abajo'

    for columna in range(1, n_columnas):
        s[0][columna] = s[0][columna - 1] + derecha[0][columna - 1]
        d[0][columna] = 'derecha'

    # 3. Calculamos el resto de la matriz de soluciones
    for columna in range(1, n_columnas):
        for fila in range(1, n_filas):
            beneficio_abajo = s[fila - 1][columna] + abajo[fila - 1][columna]
            beneficio_derecha = s[fila][columna - 1] + derecha[fila][columna - 1]

            s[fila][columna] = max(beneficio_abajo, beneficio_derecha)

            if s[fila][columna] == beneficio_abajo:
                d[fila][columna] = 'abajo'
            else:
                d[fila][columna] = 'derecha'

    # 4. Reconstruye el camino

    camino = deque([])
    # Ejercicio de clase
    # Utilizamos dos índices para recorrer "d" (decisiones)
    # Inicialmente las situamos en la esquina inferior derecha
    
    fila = n_filas - 1
    columna = n_columnas - 1

    pasos = n_filas + n_columnas - 2

    for i in range(pasos):
        if d[fila][columna] == "abajo":
            fila = fila - 1
            camino.appendleft("abajo")
        else:
            columna = columna - 1
            camino.appendleft("derecha")

    # 5. La solución es el beneficio y el camino
    return (s[n_filas - 1][n_columnas - 1], camino)


# Prueba

abajo = [
  [1, 0, 2, 4, 3],
  [4, 6, 5, 2, 1],
  [4, 4, 5, 2, 1],
  [5, 6, 8, 5, 3]
]
derecha = [
  [3, 2, 4, 0],
  [3, 2, 4, 2],
  [0, 7, 3, 4],
  [3, 3, 0, 2],
  [1, 3, 2, 2]
]
resultado, camino = manhattan_dinamica2(5, 5, abajo, derecha)
print(resultado)
print(camino)


